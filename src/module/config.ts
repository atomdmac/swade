export class SWADE {
    public static ASCII = `███████╗██╗    ██╗ █████╗ ██████╗ ███████╗
██╔════╝██║    ██║██╔══██╗██╔══██╗██╔════╝
███████╗██║ █╗ ██║███████║██║  ██║█████╗  
╚════██║██║███╗██║██╔══██║██║  ██║██╔══╝  
███████║╚███╔███╔╝██║  ██║██████╔╝███████╗
╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═════╝ ╚══════╝`;

    public static attributes = {
        agility: "SWADE.AttrAgi",
        smarts: "SWADE.AttrSma",
        spirit: "SWADE.AttrSpr",
        strength: "SWADE.AttrStr",
        vigor: "SWADE.AttrVig"
    };

    public static imagedrop = {
        height: 200
    }
}